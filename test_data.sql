insert into types
  (name)
  values ('phone')
       , ('email')
       , ('name')
       , ('address');

/*
graph=> select * from types;
 id |  name   |            created            
----+---------+-------------------------------
  1 | phone   | 2024-02-14 11:33:16.655938-08
  2 | email   | 2024-02-15 14:26:47.917329-08
  3 | name    | 2024-02-15 14:26:47.917329-08
  4 | address | 2024-02-15 14:26:47.917329-08
(4 rows)
*/

insert into nodes
  (type_id, normalized, pretty)
  values (1, '{"phone": "6265551234"}', '{"phone": "626-555-1234"}')
       , (1, '{"phone": "6265551235"}', '{"phone": "626-555-1235"}')
       , (1, '{"phone": "6265552345"}', '{"phone": "626-555-2345"}')
       , (1, '{"phone": "6265553456"}', '{"phone": "626-555-3456"}')
       , (1, '{"phone": "6265554567"}', '{"phone": "626-555-4567"}')
;

insert into nodes
  (type_id, normalized, pretty)
  values (3, '{"name": {"first": "JOHN", "last": "SMITH"}}', '{"name": {"first": "John", "last": "Smith"}}')
;

/*
graph=> select id, normalized from nodes;
 id |                  normalized                  
----+----------------------------------------------
  3 | {"phone": "6265551234"}
  4 | {"phone": "6265551235"}
  5 | {"phone": "6265552345"}
  6 | {"phone": "6265553456"}
  7 | {"phone": "6265554567"}
  8 | {"name": {"last": "SMITH", "first": "JOHN"}}
(6 rows)
*/

insert into edges
  (from_node_id, to_node_id, probability)
  values (8, 3, 0.9)
       , (3, 8, 0.9)
       , (8, 4, 0.85)
       , (4, 8, 0.85)
       , (8, 5, 0.875)
       , (5, 8, 0.875)
       , (8, 6, 0.9)
       , (6, 8, 0.9)
       , (8, 7, 0.8)
       , (7, 8, 0.8)
;

/*
graph=> select * from edges;
 id | from_node_id | to_node_id | probability |           created            
----+--------------+------------+-------------+------------------------------
  1 |            8 |          3 |         0.9 | 2024-02-15 14:37:02.76293-08
  2 |            3 |          8 |         0.9 | 2024-02-15 14:37:02.76293-08
  3 |            8 |          4 |        0.85 | 2024-02-15 14:37:02.76293-08
  4 |            4 |          8 |        0.85 | 2024-02-15 14:37:02.76293-08
  5 |            8 |          5 |       0.875 | 2024-02-15 14:37:02.76293-08
  6 |            5 |          8 |       0.875 | 2024-02-15 14:37:02.76293-08
  7 |            8 |          6 |         0.9 | 2024-02-15 14:37:02.76293-08
  8 |            6 |          8 |         0.9 | 2024-02-15 14:37:02.76293-08
  9 |            8 |          7 |         0.8 | 2024-02-15 14:37:02.76293-08
 10 |            7 |          8 |         0.8 | 2024-02-15 14:37:02.76293-08
(10 rows)
*/

/*
select count(1)
 from  edges
 where from_node_id = 8
  and  node_to_type(to_node_id) = 1;
 count 
-------
     5
(1 row)
*/

update edges
  set  probability = probability/5
  where from_node_id = 8
  and  node_to_type(to_node_id) = 1;

/*
graph=> select * from edges order by id;
 id | from_node_id | to_node_id |     probability     |           created            
----+--------------+------------+---------------------+------------------------------
  1 |            8 |          3 |                0.18 | 2024-02-15 14:37:02.76293-08
  2 |            3 |          8 |                 0.9 | 2024-02-15 14:37:02.76293-08
  3 |            8 |          4 | 0.16999999999999998 | 2024-02-15 14:37:02.76293-08
  4 |            4 |          8 |                0.85 | 2024-02-15 14:37:02.76293-08
  5 |            8 |          5 |               0.175 | 2024-02-15 14:37:02.76293-08
  6 |            5 |          8 |               0.875 | 2024-02-15 14:37:02.76293-08
  7 |            8 |          6 |                0.18 | 2024-02-15 14:37:02.76293-08
  8 |            6 |          8 |                 0.9 | 2024-02-15 14:37:02.76293-08
  9 |            8 |          7 |                0.16 | 2024-02-15 14:37:02.76293-08
 10 |            7 |          8 |                 0.8 | 2024-02-15 14:37:02.76293-08
(10 rows)
*/

insert into nodes
  (type_id, normalized, pretty)
  values (2, '{"email": "JOHN.SMITH.1234@EXAMPLE.COM"}', '{"email": "john.smith.1234@example.com"}')
       , (2, '{"email": "JOHN.SMITH.1235@EXAMPLE.COM"}', '{"email": "john.smith.1235@example.com"}')
       , (2, '{"email": "JOHN.SMITH.2345@EXAMPLE.COM"}', '{"email": "john.smith.2345@example.com"}')
       , (2, '{"email": "JOHN.SMITH.3456@EXAMPLE.COM"}', '{"email": "john.smith.3456@example.com"}')
       , (2, '{"email": "JOHN.SMITH.4567@EXAMPLE.COM"}', '{"email": "john.smith.4567@example.com"}')
;

/*
graph=> select id, type_id, normalized from nodes where type_id = 2;
 id | type_id |                normalized                
----+---------+------------------------------------------
 10 |       2 | {"email": "JOHN.SMITH.1234@EXAMPLE.COM"}
 11 |       2 | {"email": "JOHN.SMITH.1235@EXAMPLE.COM"}
 12 |       2 | {"email": "JOHN.SMITH.2345@EXAMPLE.COM"}
 13 |       2 | {"email": "JOHN.SMITH.3456@EXAMPLE.COM"}
 14 |       2 | {"email": "JOHN.SMITH.4567@EXAMPLE.COM"}
(5 rows)
*/

insert into edges
  (from_node_id, to_node_id, probability)
  values (3, 10, 0.95)
       , (10, 3, 0.95)
       , (4, 11, 0.95)
       , (11, 4, 0.95)
       , (5, 12, 0.95)
       , (12, 5, 0.95)
       , (6, 13, 0.95)
       , (13, 6, 0.95)
       , (7, 14, 0.95)
       , (14, 7, 0.95)
;

-- add edges between John Smith and only one email address
-- should demonstrate the cross-contamination problem
insert into edges
  (from_node_id, to_node_id, probability)
  values (8, 10, 0.99)
       , (10, 8, 0.99)
;
