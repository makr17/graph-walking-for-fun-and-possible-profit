with recursive neighborhood (node_id, type_id, path, p) as (
  select
    id as node_id
    , type_id
    , array_append('{}'::bigint[], id) as path
    , 1.0::double precision as p
  from nodes
  where normalized @> '{"phone":"6265551235"}' -- starting node
  union all
  select
    to_node_id as node_id
    , node_to_type(e.to_node_id)
    , array_append(nbr.path, to_node_id)
    , nbr.p * e.probability
  from
    edges e
    join neighborhood nbr
      on e.from_node_id = nbr.node_id
     and e.probability >= 0.45/nbr.p      -- stay above min p, e.p * nbr.p >= 0.45
     and not e.to_node_id = any(nbr.path) -- avoid cycles
  where
    nbr.type_id in (select id from types where identifying)
)
select
  nbr.node_id
  , n.pretty
  , max(nbr.p)
from
  neighborhood nbr
  join nodes n on nbr.node_id = n.id
group by 1,2
;
