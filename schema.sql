create table types (
  id       bigserial    not null  primary key,
  name     varchar(64)  not null,
  created  timestamptz  not null  default current_timestamp
);

alter table types
  add column identifying boolean default false;

update  types
  set   identifying = true
  where name in ('email', 'phone', 'facebook', 'twitter', ... , 'address');


create table nodes (
  id          bigserial    not null  primary key,
  type_id     bigint       not null  references types(id),
  normalized  jsonb        not null,
  pretty      jsonb        not null,
  created     timestamptz  not null  default current_timestamp
);

create unique index on nodes (md5(normalized::text));
create index normalized_idx on nodes using gin(normalized jsonb_path_ops);


create table edges (
  id            bigserial        not null  primary key,
  from_node_id  bigint           not null  references nodes(id),
  to_node_id    bigint           not null  references nodes(id),
  unique(from_node_id, to_node_id)
  probability  double precision  not null
    check(probability > 0.0 and probability < 1.0),
  created      timestamptz       not null  default current_timestamp,
);

create unique index on edges (from_node_id, to_node_id);
create index on edges (from_node_id, probability desc);

create or replace function node_to_type( _node_id bigint ) returns bigint as $$
select type_id from nodes where id = _node_id
$$ immutable language sql;

create index on edges (from_node_id, node_to_type(to_node_id));


create table sources (
  id                bigserial         not null  primary key,
  base_probability  double precision  not null
    check(base_probability > 0.0 and base_probability < 1.0),
  name              varchar(64)       not null,
  uri               varchar           not null,
  created           timestamptz       not null  default current_timestamp
);


create table edge_sources (
  id                bigserial         not null  primary key,
  edge_id           bigint            not null  references edges(id),
  source_id         bigint            not null  references sources(id),
  base_probability  double precision  not null
    check(base_probability > 0.0 and base_probability < 1.0),
  uri               varchar           not null,
  created           timestamptz       not null  default current_timestamp
);

create unique index on edge_sources (edge_id, source_id);
